# learn-git

This repository aims at learning how to use git and GitHub or GitLab in an efficient manner.

## Git workflow

### 0) Set up

All this process have to be done only once, when starting to work collaboratively. First, you have to create 
a repository to work on [GitHub](https://help.github.com/articles/create-a-repo/) or a [GitLab project](https://docs.gitlab.com/ee/gitlab-basics/create-project.html) (in this case, jump directly to step __b__).
The following step refer to how to start working in a project already created and shared with other people.

#### a) __Fork__: copy the main repository in your online account.

From a shared repository, create a fork within GitHub or GitLab. This will create a copy of the main repository
in your GitHub/GitLab account.

#### b) __Clone__: create a local copy of your fork in your computer.

Given you created a fork, you must make a copy of that in your computer. The following command will copy all files
and keep track of all modifications made in the online fork.

```
# Change to the folder where you will save files
cd path/to/folder

# Clone
git clone https://github.com/bniebuhr/jaguar-codes.git
```

#### c) Create a link between the local copy and the main repository (not the fork)

Current permissions of write and read, from your local copy to online versions of the repository:

```
# List remote repos
git remote -v
```

Add the main repository to the `remote`'s list:

```
# Add new remote link called 'upstram' (standard name for the main repository)
git remote add upstream https://github.com/piLaboratory/jaguar-codes.git

# Verify
git remote -v
```

### Making changes

Before starting to work on your code, sinchronize your local copy with the original repository, to account for 
modifications made by other people.

```
git pull upstram master

git pull origin # probably not necessary
```

Make modifications in the files.
Then, at any time it is possible to check which files were made. To do so:

```
git status
```

Add files to be commited (uploaded to the online fork).

```
# A given file
git add file

# To add all files modified
git add *
```

Commit changes

```
git commit -m 'message to identify and track the changes'
```

Send modification to my fork

```
git push origin
```

Make a pull request (directly on GitHub or GitLab website).

After analysis by other people, merge the pull request.



